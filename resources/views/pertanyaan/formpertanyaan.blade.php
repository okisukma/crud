@extends('layout.master')

@section('content')
    


    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">FORM PERTANYAAN</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan" method="POST">
            @csrf
        <div class="card-body"> 
            <div class="form-group">
                <label for="judul">judul pertanyaan</label>
                <input type="text" class="form-control" id="judul" name="judul" value="{{old("judul"," ")}}" placeholder="Enter judul pertanyaan">
               @error('judul')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror 
            </div>
                <div class="form-group">
                <label for="isi">pertanyaan</label>
                <input type="text" class="form-control" id="isi" name="isi" value="{{old("isi"," ")}}" placeholder="isi pertanyaan">
                @error('isi')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror 
            </div>
        
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
        </form>
    </div>
@endsection